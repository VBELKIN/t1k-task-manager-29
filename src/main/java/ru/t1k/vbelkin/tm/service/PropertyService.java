package ru.t1k.vbelkin.tm.service;

import com.jcabi.manifests.Manifests;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1k.vbelkin.tm.api.service.IPropertyService;

import java.util.Properties;

public class PropertyService implements IPropertyService {

    public final String FILE_NAME = "application.properties";

    public final String APPLICATION_VERSION_KEY = "buildNumber";

    public final String PASSWORD_SECRET_KEY = "password.secret";

    public final String PASSWORD_SECRET_DEFAULT = "1234";

    public final String PASSWORD_ITERATION_KEY = "password.iteration";

    public final String PASSWORD_ITERATION_DEFAULT = "5678";

    public final String AUTHOR_NAME_KEY = "developer";

    public final String AUTHOR_EMAIL_KEY = "email";

    public final String EMPTY_VALUE = "---";

    public final Properties properties = new Properties();

    @SneakyThrows
    public PropertyService() {
        properties.load(ClassLoader.getSystemResourceAsStream(FILE_NAME));
    }

    @Override
    public @NotNull String getPasswordSecret() {
        return getStringValue(PASSWORD_SECRET_KEY, PASSWORD_SECRET_DEFAULT);
    }

    @Override
    public @NotNull Integer getPasswordIteration() {
        return Integer.parseInt(getStringValue(PASSWORD_ITERATION_KEY, PASSWORD_ITERATION_DEFAULT));
    }

    @Override
    public @NotNull String getAuthorName() {
        return Manifests.read(AUTHOR_NAME_KEY);
    }

    @Override
    public @NotNull String getAuthorEmail() {
        return Manifests.read(AUTHOR_EMAIL_KEY);
    }

    @Override
    public @NotNull String getApplicationVersion() {
        return Manifests.read(APPLICATION_VERSION_KEY);
    }

    @NotNull
    private String getStringValue(@NotNull final String key) {
        return getStringValue(key, EMPTY_VALUE);
    }

    @NotNull
    private String getStringValue(@NotNull final String key, @NotNull final String defaultValue) {
        if (System.getProperties().containsKey(key)) return System.getProperties().getProperty(key);
        @NotNull final String envKey = getEnvKey(key);
        if (System.getenv().containsKey(envKey)) return System.getenv(envKey);
        return properties.getProperty(key, defaultValue);
    }

    private String getEnvKey(@NotNull final String key) {
        return key.replace(".", "_").toUpperCase();
    }

}

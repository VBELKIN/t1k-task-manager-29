package ru.t1k.vbelkin.tm.exception.role;

public class AccessDeniedException extends AbstractUserException {

    public AccessDeniedException() {
        super("Error! You are not logged in. Please log in and try again...");
    }

}

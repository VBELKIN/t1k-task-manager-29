package ru.t1k.vbelkin.tm.exception.entity;

public class TaskNotFoundException extends AbstractEntityNotFoundException {

    public TaskNotFoundException() {
        super("Error! Task not found...");
    }

}

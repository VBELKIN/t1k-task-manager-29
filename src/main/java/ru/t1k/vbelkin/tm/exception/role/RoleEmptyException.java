package ru.t1k.vbelkin.tm.exception.role;

public class RoleEmptyException extends AbstractUserException {

    public RoleEmptyException() {
        super("Error! Role is empty...");
    }

}
